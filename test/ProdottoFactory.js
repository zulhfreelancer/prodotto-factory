var pf = artifacts.require("ProdottoFactory");

contract('ProdottoFactory', function(accounts) {
  var pfInstance;

  before(function() {
    return pf.new()
      .then(function(instance) {
        pfInstance = instance;
      });
  });

  it("should return foo", function() {
    return pfInstance.foo.call()
      .then(function(str) {
        assert.equal(str, "foo");
      });
  });
});
